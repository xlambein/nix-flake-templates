{
  description = "A collection of flake templates";

  outputs = {self}: {
    templates = {
      devShell = {
        path = ./devShell;
        description = "A local environment, with a devShell and a .envrc.";
      };

      haskell = {
        path = ./haskell;
        description = "A Haskell project built with cabal2nix.";
      };

      rust = {
        path = ./rust;
        description = "A Rust project built with cargo, supplied by oxalica/rust-overlay.";
      };

      bevy = {
        path = ./bevy;
        description = "A Rust Bevy project built with cargo.";
      };

      jupyter = {
        path = ./jupyter;
        description = "A Python jupyter notebook project.";
      };
    };

    defaultTemplate = self.templates.devShell;
  };
}

{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.05";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
        };

        my-project = pkgs.haskellPackages.callCabal2nix "my-project" ./. { };
      in
      {
        packages = {
          inherit my-project;
          default = my-project;
        };

        apps = {
          my-project = { type = "app"; program = "${my-project}/bin/my-project"; };
          default = self.apps.${system}.my-project;
        };

        checks = self.packages;

        devShell = pkgs.haskellPackages.shellFor {
          packages = p: [ my-project ];
          withHoogle = true;
          buildInputs = with pkgs.haskellPackages; [
            haskell-language-server
            ghcid
            cabal-install
          ];
        };
      }
    );
}

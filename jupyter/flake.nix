{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-22.05";
    flake-utils.url = "github:numtide/flake-utils";
    jupyterWith.url = "github:tweag/jupyterWith";
    jupyterWith.inputs.nixpkgs.follows = "nixpkgs";
    jupyterWith.inputs.flake-utils.follows = "flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, jupyterWith }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          system = system;
          config.allowBroken = true;
          overlays = nixpkgs.lib.attrValues jupyterWith.overlays;
        };

        iPython = pkgs.kernels.iPythonWith {
          name = "Python";
          packages = p: with p; [ 
            numpy pandas matplotlib
            # Add Python packages here
          ];
          ignoreCollisions = true;
        };

        # iHaskell = pkgs.kernels.iHaskellWith {
        #   name = "haskell";
        #   packages = p: with p; [ ];
        # };

        jupyterEnvironment = pkgs.jupyterlabWith {
          kernels = [ iPython ];
          # kernels = [ iHaskell ];
        };
      in
      rec {
        apps.jupyterlab = {
          type = "app";
          program = "${jupyterEnvironment}/bin/jupyter-lab";
        };
        apps.default = apps.jupyterlab;
        devShell = jupyterEnvironment.env;
      }
    );
}
